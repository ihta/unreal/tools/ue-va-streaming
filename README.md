# PixelStreaming + VA Streaming Dashboard

## Introduction
[PixelStreaming](https://docs.unrealengine.com/5.0/en-US/pixel-streaming-in-unreal-engine/) is UnrealEngine's native game streaming technology. With it, it's possible to run an UnrealEngine project on one machine, while streaming its content to and recieving control input from clients over the internet connected via a browser. 

This project functions as an admin control dashboard for such streaming applications while also allowing for streaming of third-party audio comming from VA Server (bundled with this project).

## Setup
For a full and detailed tutorial, refer to the Getting-Started-Page in this repository's wiki.

After cloning the repository, open the `Server` directory and run `start.bat`. On first start-up, the script will install all necessary dependencies and then prompt you to enter a username and passwort. Those credentials are later used to log in to the dashboard and are safed in hashed and salted form in `config/users.json`. After that, the control server starts.
The Server listens on port `7273`. To open the dashboard, navigate to [localhost:7273](http://localhost:7273) in your browser and log in.

## Adding users
Additional login credentials can be created by executing `newUser.bat` and will also be saved in `config/users.json`.

## Updating VA
To allow control via the dashboard frontend, all paths in `VA/conf/VACore.ini`should feature a leading `VA/`.

e.g: 
|  |  |  |
|--|--|--|
| `voices_dir = data/voices` | ➡️ |  `voices_dir = VA/data/voices` |
| `conf_dir = conf` | ➡️ | `conf_dir = VA/conf` |

## Technology

### Frontend
The dashboard and login page are built with _TypeScript_ using [Vue.js 2](https://vuejs.org/) and [Vuetify](https://vuetifyjs.com/en/).
The code is located in `Client/ue-va-streaming-dashboard`. To deploy changes to the server, run `npm run build` inside that directory and afterwards copy all files in the `dist` folder to `Server/www`.

### Backend
The server is written in _JavaScript_ using [Express.js](https://expressjs.com/de/) and runs on [Node.js](https://nodejs.org/en/). `server.js` contains the complete API logic, `scripts/setupUser.js` is a CLI tool to create login credentials.
