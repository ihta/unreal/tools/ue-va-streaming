const express = require("express");
const path = require("path");
const http = require("http");
const cors = require("cors");
const cookieParser = require('cookie-parser');
const fs = require("fs");
const crypto = require("crypto");

const killall = require("tree-kill");

const { Server } = require("socket.io");
const { spawn } = require("node:child_process");
const { hashPassword } = require("./scripts/hashpwd");
const { kill } = require("process");


/*********************************
 *       Helper Functions        *
 *                               *
 *********************************/
function genAuthToken() {
    return crypto.randomBytes(32).toString('hex');
}

function getTimeStamp() {
    return Math.floor(Date.now() / 1000);
}

let users = [];
let userDefinedPanels = [];

try {
    users = JSON.parse(fs.readFileSync("./config/users.json").toString());
} catch {
    console.log('\x1b[31m%s\x1b[0m', '\n\nWarning: No users registered! Run setup.bat to register users..\n\n');
}

try {
    userDefinedPanels = JSON.parse(fs.readFileSync("./config/ui-config.json").toString());
} catch {
    console.log('\x1b[31m%s\x1b[0m', "Warning: Reading ui-config.json failed. No user-defined ui panels will be created");
    userDefinedPanels = [];
}

for (i = 0; i < userDefinedPanels.length; i++) {
    let panelKeys = Object.keys(userDefinedPanels[i]);

    // This abomination validates the ui-config.json. It checks if each defined panel
    // only contains the keys "title", "body" and "command" and checks, if all of them
    // are strings. If there are more or less keys defined, or a correct key is not a string
    // the if is entered and a warining is issued. In that case userDefinedPanels defaults to []
    if(!(panelKeys.includes("title") && panelKeys.includes("body") && panelKeys.includes("command") && panelKeys.includes("args") && panelKeys.length === 4 && typeof(userDefinedPanels[i].title) === 'string' && typeof(userDefinedPanels[i].body) === 'string' && typeof(userDefinedPanels[i].command) === 'string' && userDefinedPanels[i].args.constructor.name === 'Array')) {
        console.log('\x1b[31m%s\x1b[0m', "Warning: There's an error in ui-config.json and no panels will be created. Panels have to be defined as:\n\n{\n\t\"title\": <your title>,\n\t\"body\": <panel description>,\n\t\"command\": <command to execute>,\n\t\"args\": [<array of args as strings>]\n}")        
        userDefinedPanels = [];
        break;
    }

    userDefinedPanels[i].id = crypto.randomUUID();
    userDefinedPanels[i].running = false;
    userDefinedPanels[i].loading = false;
    userDefinedPanels[i].output = "";
}

console.log(userDefinedPanels);

const app = express();
const port = 7273;

/*********************************
 *          Middleware           *
 *                               *
 *********************************/
app.use(cors());
app.use(cookieParser());
app.use(express.json());

const viewPath = __dirname + '/www/';
app.use(express.static(viewPath));

// Auth-handling middleware
app.use((req, _res, next) => {
    const authToken = req.cookies['AuthToken'];
    const user = users.find((u) => { return u.authToken === authToken });

    // if corresponding user is found AND authToken is not older than 20 minutes, pass user. Else pass nothing
    user && (getTimeStamp() - user.timestamp <= 1200) ? req.user = user : req.user = undefined;
    next();
});

const requireAuth = (method = 'get') => {
    if (method === 'post') {
        return (res, req, next) => {
            if (res.user) {
                next();
            } else {
                req.status(401).send("Unauthorized api call");
                // next();
            }
        }
    } else {
        return (res, req, next) => {
            if (res.user) {
                next();
            } else {
                req.redirect(401, '/');
                // next();
            }
        }
    }
}

const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"]
    }
});

/*********************************
 *          Variables            *
 *                               *
 *********************************/
let VA = undefined;
let GAME = undefined;
let SSERVER = undefined;
let NPM = undefined;

let states = {
    va: {
        running: false,
        loading: false,
        output: ""
    },
    game: {
        running: false,
        loading: false,
        output: ""
    },
    sserver: {
        running: false,
        loading: false,
        output: ""
    }
}

let startSSERVERafterNPM = true;


/*********************************
 *             API               *
 *                               *
 *********************************/
io.on("connection", (socket) => {
    console.log("User connected: " + socket.id);
});

io.on("disconnect", () => {
    console.log("User disconnected");
});

app.get('/', (_req, res) => {
    res.sendFile(viewPath + 'rGzTZazYIDKdYdc3vYunzHlAbsfPwoXBOOHCLeLp8gPwKL9KJPoAzNImWRe0QQLRTiS5zoGJqBX5l9BWiZladGv8hZigDL7pGPy9ZUQtSRl4rFc9f78qoT9bGhUHd6At/index.html');
});

app.post('/login', (req, res) => {
    const body = req.body ? req.body : {};
    
    if(!body.user || !body.password) {
        res.status(400).send({ success: false, message: 'Bad request' });
        return;
    }

    const { user, password } = body;
    let filteredUser = users.find(u => {
        return u.user === user;
    });

    if(!filteredUser) {
        res.status(200).send({ success: false, message: 'Username not found' });
        return;
    }

    const salt = filteredUser.salt;
    const hashedPassword = hashPassword(password + salt);

    if(hashedPassword === filteredUser.password) {
        const authToken = genAuthToken();
        filteredUser.authToken = authToken;
        filteredUser.timestamp = getTimeStamp();

        res.cookie('AuthToken', authToken);
        res.status(200).send({ success: true, message: 'Login successful' });
        return;
    } else {
        res.status(200).send({ success: false, message: 'Wrong password' });
        return;
    }
});

app.get('/api/get-saved-state', requireAuth('post'), (_req, res) => {
    res.send(states);
});

app.get('/api/get-additional-panels', (_req, res) => {
    res.status(200).send(userDefinedPanels);
});

app.post('/api/new-player-peer-id', (req, res) => {
    const body = req.body ? req.body : {};

    if(!body.id) {
        res.sendStatus(400);
        return;
    }

    io.emit('newPeerID', { id: body.id });
    res.sendStatus(200);
});

app.post('/api/start-va', requireAuth('post'), (req, res) => {
    try{
        states.va.loading = true;
        VA = spawn('.\\VA\\bin\\VAServer.exe', ['localhost:12340', '.\\VA\\conf\\VACore.ini']);

        VA.stdout.on('data', data => {
            io.emit('VA', { payload: data.toString(), type: 'stdout'} );
            states.va.output += data.toString();
        });

        VA.stderr.on('data', data => {
            io.emit('VA', { payload: data.toString(), type: 'stderr'} );
            states.va.output += data.toString();
        });

        VA.on('exit', code => {
            io.emit('VA', { payload: `VA terminated with code ${code}`, type: 'closed'} );
            states.va.output = "";
            states.va.running = false;
        });

        res.sendStatus(200);
        states.va.loading = false;
        states.va.running = true;
    } catch {
        io.emit('VA', { payload: "Startup failed!", type: 'stderr' });
        states.va.output = "";
        states.va.loading = false;
        res.sendStatus(400);
    }
});

app.post('/api/start-signaling-server', requireAuth('post'), (req, res) => {
    // const startSignalingServer = () => {
    try{
        //SSERVER = spawn('node', ['.\\SignalingServer\\cirrus', '%*']) This line used to start the cirrus script directly. Now a bash script is called
        SSERVER = spawn('powershell', ['.\\SignalingServer\\platform_scripts\\cmd\\Start_WithTURN_SignallingServer.ps1'])

        SSERVER.stdout.on('data', data => {
            io.emit('SSERVER', { payload: data.toString(), type: 'stdout'} );
            states.sserver.output += data.toString();
        });

        SSERVER.stderr.on('data', data => {
            io.emit('SSERVER', { payload: data.toString(), type: 'stderr'} );
            states.sserver.output += data.toString();
        });

        SSERVER.on('exit', code => {
            io.emit('SSERVER', { payload: `SSERVER terminated with code ${code}`, type: 'closed'} );
            states.sserver.running = false;
            states.sserver.output = "";
        });

        res.sendStatus(200);
        states.sserver.loading = false;
        states.sserver.running = true;
    } catch {
        io.emit('SSERVER', { payload: "Startup failed!", type: 'stderr' });
        states.sserver.output = "";
        states.sserver.loading = false;
        res.sendStatus(400);
    }
    // };
    
    /*
    try {
        states.sserver.loading = true;
        NPM = spawn('npm.cmd', ['--prefix', '.\\SignalingServer', 'install', '.\\SignalingServer']);

        NPM.stdout.on('data', data => {
            io.emit('SSERVER', { payload: data.toString(), type: 'stdout'} );
            states.sserver.output += data.toString();
        });

        NPM.stderr.on('data', data => {
            io.emit('SSERVER', { payload: data.toString(), type: 'stderr'} );
            states.sserver.output += data.toString();
        });

        NPM.on('exit', code => {
            io.emit('SSERVER', { payload: `NPM terminated with code ${code}`, type: 'stdout'} );
            states.sserver.output = "";
            if(startSSERVERafterNPM){
                startSignalingServer();
            }
        });
        
    } catch {
        io.emit('SSERVER', { payload: "Startup failed!", type: 'stderr' });
        res.sendStatus(400);
        states.sserver.output = "";
        states.sserver.loading = false;
    }
    */
});

app.post('/api/start-game', requireAuth('post'), (req, res) => {
    // TODO: Remove this line and add logic to start UE Game executable with launch parameters
    res.sendStatus(200);
});

app.post('/api/stop-va', requireAuth('post'), (req, res) => {
    if(!VA) {
        res.sendStatus(200);
        return;
    }

    try {
        VA.stdin.pause();
        VA.kill();
        res.sendStatus(200);
    } catch {
        res.sendStatus(400);
    }
});

app.post('/api/stop-signaling-server', requireAuth('post'), (req, res) => {
    /*
    if(!NPM && !SSERVER) {
        res.sendStatus(200);
        return;
    }

    try {
        startSSERVERafterNPM = false;
        
        if(NPM){
            NPM.stdin.pause();
            NPM.kill();
        }
        if(SSERVER){
            SSERVER.stdin.pause();
            SSERVER.kill();
        }

        startSSERVERafterNPM = true;
        res.sendStatus(200);
    } catch {
        res.sendStatus(400);
    }
    */
   if(!SSERVER) {
    res.sendStatus(200);
    return;
   }

   try {
        if(SSERVER) {
            SSERVER.stdin.pause();
            //SSERVER.kill();
            killall(SSERVER.pid);
        }

        res.sendStatus(200);
   } catch {
        res.sendStatus(400);
   }
});

app.post('/api/start-custom-process', (req, res) => {
    res.sendStatus(400);
});

app.post('/api/stop-custom-process', (req, res) => {
    res.sendStatus(400);
});

server.listen(port);
console.log('\x1b[32m%s\x1b[0m', '\n\nServer up, listening on port 7273');

module.exports = server;
