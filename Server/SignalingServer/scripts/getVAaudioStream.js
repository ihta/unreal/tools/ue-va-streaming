peer = new Peer()
const data = undefined;

peer.on("open", () => {
    data = { id: peer.id };
    console.log(data);
});

function startAudio() {
    //axios.get(`http://localhost:80/set-peer-id?id=${peer.id}`);

    console.log(`calling: posting my peer id (${peer.id}) to http://streaming.akustik.rwth-aachen.de/set-peer-id`);
    axios.post("http://streaming.akustik.rwth-aachen.de/set-peer-id", {id: peer.id});
    console.log("calling: posting worked");

    peer.on("call", (call) => {
        console.log("calling: incoming");
        call.answer()
        console.log("calling: answered");
        call.on("stream", (remoteStream) => {
            console.log("calling: Started stream")
            document.getElementById("va-audio-container").srcObject = remoteStream;
            document.getElementById("va-audio-container").play();
        });
        console.log("calling: registered stream callback");
    });

    console.log("calling: registerd onCall callback");
}