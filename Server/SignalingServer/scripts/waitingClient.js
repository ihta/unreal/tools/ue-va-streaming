let ws = undefined;

function connect() {
	window.WebSocket = window.WebSocket || window.MozWebSocket;

	if (!window.WebSocket) {
		alert('Your browser doesn\'t support WebSocket');
		return;
	}

	ws = new WebSocket(window.location.href.replace('http://', 'ws://').replace('https://', 'wss://'));

    ws.onmessage = function (event) {
        let msg = JSON.parse(event.data);
        if (msg.type === 'queue-position') {
            let label = document.getElementById("queue-number");
            let title = document.getElementById("tab-title");
            label.innerHTML = msg.val;
            title.innerHTML = `Waiting at position ${msg.val}`;
        } else if (msg.type === 'redirect') {
            window.location.href = msg.val;
        }
    }

    ws.onopen = function () {
	    ws.send(JSON.stringify({type: 'waiting-connection'}));
    }
}