const yargs = require("yargs");
const fs = require("fs");
const crypto = require("crypto");

const readline = require('readline');
const hashPassword = require('./hashpwd').hashPassword;

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}

function salt() {
    return crypto.randomBytes(32).toString('hex');
}

async function validateUser() {
    let username = await askQuestion("Enter a username\n");
    let password = await askQuestion("Enter a password\n");

    const filteredUsers = users.filter(usr => { return usr.user === username });

    if(filteredUsers.length > 1) {
        console.error("\nSomething is wrong with the database\n");
        process.exit(0);
    } else if(filteredUsers.length == 0) {
        console.log("\Username not found\n");
        process.exit(0);
    }

    const foundUser = filteredUsers[0];
    const hashedPassword = hashPassword(password + foundUser.salt);

    if(hashedPassword === foundUser.password) {
        console.log("\nUsername and password correct\n");
    } else {
        console.error("\nWrong password!\n");
    }
}

async function createUser() {
    let username = await askQuestion("Enter a username\n");
    let password = await askQuestion("Enter a password\n");

    let newUser = {
        user: username
    };

    if(users.filter(user => { return user.user === newUser.user }).length > 0) {
        console.error("\nUsername already taken\n");
        process.exit(0);
    }

    const newSalt = salt();
    const hashedPassword = hashPassword(password + newSalt);
    
    newUser.password = hashedPassword;
    newUser.salt = newSalt;
    
    users.push(newUser);
    fs.writeFileSync("./config/users.json", JSON.stringify(users));
}

const usage = "\nUsage: Creates or validates a user-password pair";

const options = yargs
    .usage(usage)
    .option("v", {alias: "verify", describe: "Instead of adding a user verifys a username-password combo", demandOption: false})
    .help(true)
    .version(false)
    .argv;

let users = [];

console.log("\n\n\n--------------------------------\n\nCreate a new user to access control dashboard\n\n--------------------------------\n");

try {
    users = JSON.parse(fs.readFileSync("./config/users.json").toString());
} catch {
    users = [];
    if(options.v) {
        console.error("\nNo users in database\n");
        process.exit(0);
    }
}

options.v ? validateUser() : createUser();
