@echo off
pushd %~dp0
call npm i
popd

if not exist ".\config\" (
    mkdir .\config
)

if not exist ".\VA\" (
    mkdir .\VA
)

node .\scripts\setupUser.js