export interface socketResponse {
  id?: string;
  payload?: string;
  type?: string;
}

export interface serverStateResponse {
  va: {
    running: boolean;
    loading: boolean;
    output: string;
  };
  game: {
    running: boolean;
    loading: boolean;
    output: string;
  };
  sserver: {
    running: boolean;
    loading: boolean;
    output: string;
  };
}

export interface status {
  loading: boolean;
  running: boolean;
  consoleText: string;
  error: boolean;
  errorText: string;
}

export interface Call {
  id: string;
  active: boolean;
  call: any;
}

export interface Panel {
  id: string;
  title: string;
  body: string;
  command: string;
}

export interface allData {
  openPanels: Array<number>;
  additionalPanels: Array<Panel>;
  gotAdditionalPanels: boolean;
  socket: any;
  peer: any;
  stream: any;
  useUEaudio: boolean;
  calls: Array<Call>;
  statusAll: boolean;
  statusVA: status;
  statusSignalingServer: status;
  statusAudioStream: status;
  statusGame: status;
}
