import Vue from "vue";
import { colors } from "vuetify/lib";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#f5f5f5",
        secondary: "#5e5aaa",
        danger: colors.red.lighten1,
      },
    },
  },
});
